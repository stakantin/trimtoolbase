package com.equineelectronics.trimtoolbase;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddClientActivity extends AppCompatActivity {

    private DBHandler dbHandler;
    private SQLiteDatabase mSqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client);

        dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        EditText clientNameField = (EditText) findViewById(R.id.clientNameField);

        String name = getIntent().getExtras().getString("name");
        if (name != "") {
            clientNameField.setText(name, TextView.BufferType.EDITABLE);
        }
    }

    public void AddClientButtonClick(View view) {
        EditText clientNameField = (EditText) findViewById(R.id.clientNameField);
        EditText clientEmailField = (EditText) findViewById(R.id.clientEmailField);
        EditText clientPhoneField = (EditText) findViewById(R.id.clientPhoneField);
        EditText clientCityField = (EditText) findViewById(R.id.clientCityField);
        String name = clientNameField.getText().toString();
        String email = clientEmailField.getText().toString();
        String phone = clientPhoneField.getText().toString();
        String city = clientCityField.getText().toString();

        //check if such email and name is not exist in db
        String selection = dbHandler.CLIENTS_NAME_COLUMN + " = ? OR " + dbHandler.CLIENTS_EMAIL_COLUMN + " = ?";
        String selectionArgs[] = new String[] {name, email};
        Cursor cursor = mSqLiteDatabase.query(
                dbHandler.CLIENTS_TABLE,
                new String[]{
                        BaseColumns._ID, dbHandler.CLIENTS_NAME_COLUMN, dbHandler.CLIENTS_EMAIL_COLUMN
                },
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        cursor.moveToFirst();
        cursor.close();

        Intent answerIntent = new Intent();

        if (cursor.getCount() > 0) {
            Toast.makeText(getApplicationContext(),
                    "Such user is already exist, please choose other email and/or name", Toast.LENGTH_SHORT).show();
        } else {
            ContentValues values = new ContentValues();
            values.put(dbHandler.CLIENTS_NAME_COLUMN, name);
            values.put(dbHandler.CLIENTS_EMAIL_COLUMN, email);
            values.put(dbHandler.CLIENTS_PHONE_COLUMN, phone);
            values.put(dbHandler.CLIENTS_CITY_COLUMN, city);
            mSqLiteDatabase.insert(dbHandler.CLIENTS_TABLE, null, values);

            answerIntent.putExtra("name", name);
            setResult(RESULT_OK, answerIntent);

            finish();
        }
    }
}
