package com.equineelectronics.trimtoolbase;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.DatePickerDialog.OnDateSetListener;

import java.util.Calendar;

public class AddEventActivity extends AppCompatActivity {

    private DBHandler dbHandler;
    private SQLiteDatabase mSqLiteDatabase;
    private Long horse_id;

    TextView eventDateField;

    int DIALOG_DATE = 1;
    int myYear;
    int myMonth;
    int myDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        horse_id = getIntent().getExtras().getLong("horse_id");

        dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        myYear = calendar.get(Calendar.YEAR);
        myMonth = calendar.get(Calendar.MONTH) + 1;
        myDay = calendar.get(Calendar.DAY_OF_MONTH);

        String myMonthString;
        String myDayString;

        if (myMonth < 10) {
            myMonthString = "0" + myMonth;
        } else {
            myMonthString = "" + myMonth;
        }

        if (myDay < 10) {
            myDayString = "0" + myDay;
        } else {
            myDayString = "" + myDay;
        }

        eventDateField = (TextView) findViewById(R.id.eventDatePickerField);
        eventDateField.setText(myYear + "/" + myMonthString + "/" + myDayString);

        addEventTypesSpinner();
        addEventStatusesSpinner();

    }

    public void selectDate(View view) {
        showDialog(DIALOG_DATE);
    }

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_DATE) {
            DatePickerDialog tpd = new DatePickerDialog(this, myCallBack, myYear, myMonth, myDay);
            return tpd;
        }
        return super.onCreateDialog(id);
    }

    OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String myMonthString;
            String myDayString;

            myYear = year;
            myMonth = monthOfYear + 1;
            myDay = dayOfMonth;

            if (myMonth < 10) {
                myMonthString = "0" + myMonth;
            } else {
                myMonthString = "" + myMonth;
            }

            if (myDay < 10) {
                myDayString = "0" + myDay;
            } else {
                myDayString = "" + myDay;
            }

            eventDateField.setText(myYear + "/" + myMonthString + "/" + myDayString);
        }
    };

    public void AddEventButtonClick(View view) {

        TextView eventDateField = (TextView) findViewById(R.id.eventDatePickerField);
        Spinner eventTypeField = (Spinner) findViewById(R.id.EventTypeSpinnerField);
        Spinner eventStatusField = (Spinner) findViewById(R.id.EventStatusSpinnerField);
        EditText eventAmountField = (EditText) findViewById(R.id.eventAmountField);
        EditText eventGeneralDescriptionField = (EditText) findViewById(R.id.eventGeneralDescriptionField);

        String date = eventDateField.getText().toString();
        String type = eventTypeField.getSelectedItem().toString();
        String status = eventStatusField.getSelectedItem().toString();
        String amount = eventAmountField.getText().toString();
        String generalDescription = eventGeneralDescriptionField.getText().toString();

        Intent answerIntent = new Intent();

        ContentValues values = new ContentValues();
        values.put(dbHandler.EVENTS_HORSE_ID_COLUMN, horse_id);
        values.put(dbHandler.EVENTS_DATE_COLUMN, date);
        values.put(dbHandler.EVENTS_STATUS_COLUMN, status);
        values.put(dbHandler.EVENTS_TYPE_COLUMN, type);
        values.put(dbHandler.EVENTS_AMOUNT_COLUMN, amount);
        values.put(dbHandler.EVENTS_GENERAL_DESCRIPTION_COLUMN, generalDescription);
        long event_id = mSqLiteDatabase.insert(dbHandler.EVENTS_TABLE, null, values);

        answerIntent.putExtra("event_id", event_id);
        setResult(RESULT_OK, answerIntent);

        finish();

    }

    private void addEventStatusesSpinner() {
        // адаптер
        String[] statuses = getResources().getStringArray(R.array.event_statuses);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, statuses);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner statusSpinner = (Spinner) findViewById(R.id.EventStatusSpinnerField);
        statusSpinner.setAdapter(adapter);

        // заголовок
        statusSpinner.setPrompt("Status");

        // устанавливаем обработчик нажатия
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
//                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void addEventTypesSpinner() {
        // адаптер
        String[] types = getResources().getStringArray(R.array.event_types);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner typeSpinner = (Spinner) findViewById(R.id.EventTypeSpinnerField);
        typeSpinner.setAdapter(adapter);

        // заголовок
        typeSpinner.setPrompt("Type");

        // устанавливаем обработчик нажатия
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
//                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }
}
