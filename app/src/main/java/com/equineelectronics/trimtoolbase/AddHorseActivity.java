package com.equineelectronics.trimtoolbase;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddHorseActivity extends AppCompatActivity {

    private DBHandler dbHandler;
    private SQLiteDatabase mSqLiteDatabase;
    private Long client_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_horse);

        client_id = getIntent().getExtras().getLong("client_id");

        dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        EditText horseNameField = (EditText) findViewById(R.id.horseNameField);

        String name = getIntent().getExtras().getString("name");
        if (name != "") {
            horseNameField.setText(name, TextView.BufferType.EDITABLE);
        }

        addHorseBreedSpinner();
        addHorseSexSpinner();
    }

    public void AddHorseButtonClick(View view) {
        EditText horseNameField = (EditText) findViewById(R.id.horseNameField);
        EditText horseYearOfBirthField = (EditText) findViewById(R.id.horseYearOfBirthField);
        Spinner horseBreedField = (Spinner) findViewById(R.id.horseBreedSpinnerField);
        Spinner horseSexField = (Spinner) findViewById(R.id.horseSexSpinnerField);
        String name = horseNameField.getText().toString();
        String yearOfBirth = horseYearOfBirthField.getText().toString();
        String breed = horseBreedField.getSelectedItem().toString();
        String sex = horseSexField.getSelectedItem().toString();

        //check if such name is not exist in db for specified client
        String selection = dbHandler.HORSES_NAME_COLUMN + " = ? AND " + dbHandler.HORSES_CLIENT_ID_COLUMN + " = ?";
        String selectionArgs[] = new String[] {name, "" + client_id};
        Cursor cursor = mSqLiteDatabase.query(
                dbHandler.HORSES_TABLE,
                new String[]{
                        BaseColumns._ID, dbHandler.HORSES_NAME_COLUMN
                },
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        cursor.moveToFirst();
        cursor.close();

        Intent answerIntent = new Intent();

        if (cursor.getCount() > 0) {
            Toast.makeText(getApplicationContext(),
                    "Such horse is already exist, please choose other name", Toast.LENGTH_SHORT).show();
        } else {
            ContentValues values = new ContentValues();
            values.put(dbHandler.HORSES_CLIENT_ID_COLUMN, client_id);
            values.put(dbHandler.HORSES_NAME_COLUMN, name);
            values.put(dbHandler.HORSES_YEAR_OF_BIRTH_COLUMN, yearOfBirth);
            values.put(dbHandler.HORSES_BREED_COLUMN, breed);
            values.put(dbHandler.HORSES_SEX_COLUMN, sex);
            mSqLiteDatabase.insert(dbHandler.HORSES_TABLE, null, values);

            answerIntent.putExtra("name", name);
            setResult(RESULT_OK, answerIntent);

            finish();
        }
    }

    private void addHorseBreedSpinner() {
        // адаптер
        String[] breeds = getResources().getStringArray(R.array.horse_breeds);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, breeds);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner breedSpinner = (Spinner) findViewById(R.id.horseBreedSpinnerField);
        breedSpinner.setAdapter(adapter);

        // заголовок
        breedSpinner.setPrompt("Breed");

        // устанавливаем обработчик нажатия
        breedSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
//                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void addHorseSexSpinner() {
        // адаптер
        String[] sexs = getResources().getStringArray(R.array.horse_sexs);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sexs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner sexSpinner = (Spinner) findViewById(R.id.horseSexSpinnerField);
        sexSpinner.setAdapter(adapter);

        // заголовок
        sexSpinner.setPrompt("Sex");

        // устанавливаем обработчик нажатия
        sexSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
                //Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }
}
