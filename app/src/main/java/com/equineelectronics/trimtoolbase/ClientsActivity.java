package com.equineelectronics.trimtoolbase;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class ClientsActivity extends ListActivity implements AdapterView.OnItemLongClickListener {

    private DBHandler dbHandler;
    private SQLiteDatabase mSqLiteDatabase;

    static final private int ADD_CLIENT = 0;
    static final private int MANAGE_CLIENT = 1;

    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients);

        dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        Cursor cursor = mSqLiteDatabase.query(
                dbHandler.CLIENTS_TABLE,
                new String[]{
                        BaseColumns._ID, dbHandler.CLIENTS_NAME_COLUMN
                },
                null,
                null,
                null,
                null,
                dbHandler.CLIENTS_NAME_COLUMN + " ASC"
        );

        if (cursor != null) {
            cursor.moveToFirst();
        }

        adapter = new SimpleCursorAdapter(
                getBaseContext(), // Context.
                R.layout.client_list_item,
                cursor,
                new String[]{dbHandler.CLIENTS_NAME_COLUMN},
                new int[]{R.id.clientNameField},
                0
        );

        setListAdapter(adapter);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(ClientsActivity.this, HorsesActivity.class);
        intent.putExtra("client_id", id);
        TextView clientNameField = (TextView) v.findViewById(R.id.clientNameField);
        intent.putExtra("client_name", clientNameField.getText());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(ClientsActivity.this, ManageClientActivity.class);
        intent.putExtra("client_id", id);
        startActivityForResult(intent, MANAGE_CLIENT);
        return true;
    }

    public void AddClient(View view) {
        SearchView searchView = (SearchView) findViewById(R.id.searchClientView);

        Intent intent = new Intent(ClientsActivity.this, AddClientActivity.class);
        intent.putExtra("name", searchView.getQuery().toString());
        startActivityForResult(intent, ADD_CLIENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == ADD_CLIENT || requestCode == MANAGE_CLIENT) {
            if (resultCode == RESULT_OK) {
                finish();
                Intent intent = getIntent();
                startActivity(intent);
            }
        }
    }

}
