package com.equineelectronics.trimtoolbase;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DBHandler extends SQLiteOpenHelper implements BaseColumns {
    private static final String DATABASE_NAME = "trimtool.db";
    private static final int DATABASE_VERSION = 5;

    static final String CLIENTS_TABLE = "clients";
    static final String HORSES_TABLE = "horses";
    static final String EVENTS_TABLE = "events";

    static final String CLIENTS_NAME_COLUMN = "name";
    static final String CLIENTS_PHONE_COLUMN = "phone";
    static final String CLIENTS_EMAIL_COLUMN = "email";
    static final String CLIENTS_CITY_COLUMN = "city";

    static final String HORSES_CLIENT_ID_COLUMN = "client_id";
    static final String HORSES_NAME_COLUMN = "name";
    static final String HORSES_YEAR_OF_BIRTH_COLUMN = "year_of_birth";
    static final String HORSES_BREED_COLUMN = "breed";
    static final String HORSES_SEX_COLUMN = "sex";
    static final String HORSES_IS_DECEASED_COLUMN = "is_deceased";

    static final String EVENTS_HORSE_ID_COLUMN = "horse_id";
    static final String EVENTS_DATE_COLUMN = "date";
    static final String EVENTS_STATUS_COLUMN = "status";
    static final String EVENTS_TYPE_COLUMN = "type";
    static final String EVENTS_AMOUNT_COLUMN = "amount";
    static final String EVENTS_GENERAL_DESCRIPTION_COLUMN = "general_description";

    private static final String CLIENTS_TABLE_CREATE_SCRIPT = "create table " + CLIENTS_TABLE + " ("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + CLIENTS_NAME_COLUMN + " text not null, "
            + CLIENTS_PHONE_COLUMN + " text, "
            + CLIENTS_EMAIL_COLUMN + " text, "
            + CLIENTS_CITY_COLUMN + " text);";

    private static final String HORSES_TABLE_CREATE_SCRIPT = "create table " + HORSES_TABLE + " ("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + HORSES_CLIENT_ID_COLUMN + " integer not null, "
            + HORSES_NAME_COLUMN + " text not null, "
            + HORSES_YEAR_OF_BIRTH_COLUMN + " integer, "
            + HORSES_BREED_COLUMN + " text, "
            + HORSES_SEX_COLUMN + " text,"
            + HORSES_IS_DECEASED_COLUMN + " INTEGER DEFAULT 0);";

    private static final String EVENTS_TABLE_CREATE_SCRIPT = "create table " + EVENTS_TABLE + " ("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + EVENTS_HORSE_ID_COLUMN + " integer not null, "
            + EVENTS_DATE_COLUMN + " text not null, "
            + EVENTS_STATUS_COLUMN + " text not null, "
            + EVENTS_TYPE_COLUMN + " text not null, "
            + EVENTS_AMOUNT_COLUMN + " float, "
            + EVENTS_GENERAL_DESCRIPTION_COLUMN + " text);";

    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CLIENTS_TABLE_CREATE_SCRIPT);
        db.execSQL(HORSES_TABLE_CREATE_SCRIPT);
        db.execSQL(EVENTS_TABLE_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 5) {
            db.execSQL("ALTER TABLE " + HORSES_TABLE + " ADD COLUMN " + HORSES_IS_DECEASED_COLUMN + " INTEGER DEFAULT 0;");
        }
    }

    DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}