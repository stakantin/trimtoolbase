package com.equineelectronics.trimtoolbase;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class EventActivity extends ListActivity implements AdapterView.OnItemLongClickListener {

    private DBHandler dbHandler;
    private SQLiteDatabase mSqLiteDatabase;
    public Long horse_id;
    public String horse_name;
    private Bundle extras;

    static final private int ADD_EVENT = 20;
    static final private int MANAGE_EVENT = 21;

    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("horse_id") && extras.containsKey("horse_name")) {
            horse_id = getIntent().getExtras().getLong("horse_id");
            horse_name = getIntent().getExtras().getString("horse_name");
        } else {
            horse_name = getResources().getString(R.string.all);
        }

        TextView clientNameField = (TextView) findViewById(R.id.eventsListHeader);
        clientNameField.append(horse_name);

        //Get record from db
        dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        //Select horses of current client
        String table = dbHandler.EVENTS_TABLE + " as EV left join " + dbHandler.HORSES_TABLE + " as HR on EV." + dbHandler.EVENTS_HORSE_ID_COLUMN + " = HR." + BaseColumns._ID;
        String columns[] = new String[]{
                "EV." + BaseColumns._ID,
                "EV." + dbHandler.EVENTS_DATE_COLUMN + " as " + dbHandler.EVENTS_DATE_COLUMN,
                "EV." + dbHandler.EVENTS_TYPE_COLUMN + " as " + dbHandler.EVENTS_TYPE_COLUMN,
                "EV." + dbHandler.EVENTS_STATUS_COLUMN + " as " + dbHandler.EVENTS_STATUS_COLUMN,
                "HR." + dbHandler.HORSES_NAME_COLUMN + " as horse_name",
        };
        String selection = null;
        String selectionArgs[] = null;
        if (extras != null && extras.containsKey("horse_id")) {
            selection = dbHandler.EVENTS_HORSE_ID_COLUMN + " = ?";
            selectionArgs = new String[]{"" + horse_id};
        }

        Cursor cursor = mSqLiteDatabase.query(
                table,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                dbHandler.EVENTS_DATE_COLUMN + " DESC"
        );

        if (cursor != null) {
            cursor.moveToFirst();
        }

        adapter = new SimpleCursorAdapter(
                getBaseContext(), // Context.
                R.layout.event_list_item,
                cursor,
                new String[]{
                        dbHandler.EVENTS_DATE_COLUMN,
                        dbHandler.EVENTS_TYPE_COLUMN,
                        dbHandler.EVENTS_STATUS_COLUMN,
                        "horse_name"
                },
                new int[]{
                        R.id.eventDateField,
                        R.id.eventTypeField,
                        R.id.eventStatusField,
                        R.id.horseNameField,
                },
                0
        );

        setListAdapter(adapter);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long event_id) {
        super.onListItemClick(l, v, position, event_id);

        Intent intent = new Intent(EventActivity.this, ManageEventActivity.class);
        intent.putExtra("event_id", event_id);
        startActivityForResult(intent, MANAGE_EVENT);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return true;
    }

    public void AddEvent(View view) {
        if (extras != null && extras.containsKey("horse_id")) {
            SearchView searchView = (SearchView) findViewById(R.id.searchEventsView);

            Intent intent = new Intent(EventActivity.this, AddEventActivity.class);
            intent.putExtra("horse_id", horse_id);
            intent.putExtra("date", searchView.getQuery().toString());
            startActivityForResult(intent, ADD_EVENT);
        } else {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.cannot_add_an_event), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        finish();
        Intent intent = getIntent();
        startActivity(intent);
    }
}
