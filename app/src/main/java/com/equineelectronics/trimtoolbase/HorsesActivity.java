package com.equineelectronics.trimtoolbase;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class HorsesActivity extends ListActivity implements AdapterView.OnItemLongClickListener {

    public Long client_id;
    public String client_name;
    private Bundle extras;

    static final private int ADD_HORSE = 10;
    static final private int MANAGE_HORSE = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horses);

        extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("client_id") && extras.containsKey("client_name")) {
            client_id = extras.getLong("client_id");
            client_name = extras.getString("client_name");
        } else {
            client_name = getResources().getString(R.string.all);
        }

        TextView clientNameField = findViewById(R.id.horsesListHeader);
        clientNameField.append(client_name);

        //Get horses from db
        DBHandler dbHandler = new DBHandler(this);
        SQLiteDatabase mSqLiteDatabase = dbHandler.getWritableDatabase();

        //Select horses of current client if set.
        String query = "SELECT " +
                "HR." + BaseColumns._ID +
                ",HR." + DBHandler.HORSES_NAME_COLUMN + " as " + DBHandler.HORSES_NAME_COLUMN +
                ",HR." + DBHandler.HORSES_BREED_COLUMN + " as " + DBHandler.HORSES_BREED_COLUMN +
                ",HR." + DBHandler.HORSES_YEAR_OF_BIRTH_COLUMN + " as " + DBHandler.HORSES_YEAR_OF_BIRTH_COLUMN +
                ",CL." + DBHandler.CLIENTS_NAME_COLUMN + " as owner_name" +
                ",HR." + DBHandler.HORSES_IS_DECEASED_COLUMN + " as " + DBHandler.HORSES_IS_DECEASED_COLUMN +
                ",(" +
                "SELECT " + DBHandler.EVENTS_DATE_COLUMN +
                " FROM " + DBHandler.EVENTS_TABLE + " as EV" +
                " WHERE EV." + DBHandler.EVENTS_HORSE_ID_COLUMN + " = HR." + BaseColumns._ID +
                " ORDER BY " + DBHandler.EVENTS_DATE_COLUMN + " DESC LIMIT 1" +
                ") as last_event_date" +
                " FROM " + DBHandler.HORSES_TABLE + " as HR" +
                " LEFT JOIN " + DBHandler.CLIENTS_TABLE + " as CL on HR." + DBHandler.HORSES_CLIENT_ID_COLUMN + " = CL." + BaseColumns._ID;

        String horses_selectionArgs[] = null;
        if (extras != null && extras.containsKey("client_id")) {
            query += " where " + DBHandler.HORSES_CLIENT_ID_COLUMN + " = ?";
            horses_selectionArgs = new String[]{"" + client_id};
        }

        Cursor horses_cursor = mSqLiteDatabase.rawQuery(
                query,
                horses_selectionArgs
        );

        if (horses_cursor != null) {
            horses_cursor.moveToFirst();
            Log.v("Cursor Object", horses_cursor.toString());
        }

        HorsesCursorAdapter adapter = new HorsesCursorAdapter(
                getBaseContext(),
                R.layout.horse_list_item,
                horses_cursor,
                new String[]{
                        DBHandler.HORSES_NAME_COLUMN,
                        DBHandler.HORSES_BREED_COLUMN,
                        DBHandler.HORSES_YEAR_OF_BIRTH_COLUMN,
                        DBHandler.HORSES_IS_DECEASED_COLUMN,
                        "owner_name",
                        "last_event_date"
                },
                new int[]{
                        R.id.horseNameField,
                        R.id.horseBreedField,
                        R.id.horseYearOfBirthField,
                        R.id.horseOwnerNameField,
                },
                0
        );

        setListAdapter(adapter);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(HorsesActivity.this, EventActivity.class);
        intent.putExtra("horse_id", id);
        TextView horseNameField = v.findViewById(R.id.horseNameField);
        intent.putExtra("horse_name", horseNameField.getText());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(HorsesActivity.this, ManageHorseActivity.class);
        intent.putExtra("horse_id", id);
        startActivityForResult(intent, MANAGE_HORSE);
        return true;
    }

    public void AddHorse(View view) {
        if (extras != null && extras.containsKey("client_id")) {
            SearchView searchView = findViewById(R.id.searchHorsesView);

            Intent intent = new Intent(HorsesActivity.this, AddHorseActivity.class);
            intent.putExtra("client_id", client_id);
            intent.putExtra("name", searchView.getQuery().toString());
            startActivityForResult(intent, ADD_HORSE);
        } else {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.cannot_add_a_horse), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == ADD_HORSE || requestCode == MANAGE_HORSE) {
            if (resultCode == RESULT_OK) {
                finish();
                Intent intent = getIntent();
                startActivity(intent);
            }
        }
    }
}
