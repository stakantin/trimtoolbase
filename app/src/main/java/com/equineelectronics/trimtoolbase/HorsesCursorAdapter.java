package com.equineelectronics.trimtoolbase;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HorsesCursorAdapter extends SimpleCursorAdapter {
    private int layout;
    private final LayoutInflater inflater;

    HorsesCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.layout = layout;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        TextView horseNameField = view.findViewById(R.id.horseNameField);
        TextView horseOwnerNameField = view.findViewById(R.id.horseOwnerNameField);
        TextView horseBreedField = view.findViewById(R.id.horseBreedField);
        TextView horseYearOfBirthField = view.findViewById(R.id.horseYearOfBirthField);

        int horseName = cursor.getColumnIndexOrThrow(DBHandler.HORSES_NAME_COLUMN);
        int horseBreed = cursor.getColumnIndexOrThrow(DBHandler.HORSES_BREED_COLUMN);
        int horseYearOfBirth = cursor.getColumnIndexOrThrow(DBHandler.HORSES_YEAR_OF_BIRTH_COLUMN);
        int ownerName = cursor.getColumnIndexOrThrow("owner_name");
        int isDeceased = cursor.getColumnIndexOrThrow(DBHandler.HORSES_IS_DECEASED_COLUMN);
        int lastEventDate = cursor.getColumnIndexOrThrow("last_event_date");

        horseNameField.setText(cursor.getString(horseName));
        horseOwnerNameField.setText(cursor.getString(ownerName));
        horseBreedField.setText(cursor.getString(horseBreed));
        horseYearOfBirthField.setText(cursor.getString(horseYearOfBirth));

        view.setBackgroundColor(context.getResources().getColor(R.color.backgroundWhite));
        horseNameField.setTextColor(context.getResources().getColor(R.color.textGrey));

        Integer isHorseDeceased = cursor.getInt(isDeceased);
        if (isHorseDeceased == 1) {
            view.setBackgroundColor(context.getResources().getColor(R.color.backgroundGray));
        } else if (cursor.getString(lastEventDate) != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
            try {
                Date date = sdf.parse(cursor.getString(lastEventDate));

                Calendar plusSixWeeksCalendar = Calendar.getInstance();
                Calendar plusEightWeeksCalendar = Calendar.getInstance();
                Calendar nowCalendar = Calendar.getInstance();

                plusSixWeeksCalendar.setTime(date);
                plusSixWeeksCalendar.add(Calendar.DATE, 7 * 6);

                plusEightWeeksCalendar.setTime(date);
                plusEightWeeksCalendar.add(Calendar.DATE, 7 * 8);

                if (plusEightWeeksCalendar.getTime().before(nowCalendar.getTime())) {
                    horseNameField.setTextColor(context.getResources().getColor(R.color.textRed));
                } else if (plusSixWeeksCalendar.getTime().before(nowCalendar.getTime())) {
                    horseNameField.setTextColor(context.getResources().getColor(R.color.textYellow));
                } else {
                    horseNameField.setTextColor(context.getResources().getColor(R.color.textGreen));

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
