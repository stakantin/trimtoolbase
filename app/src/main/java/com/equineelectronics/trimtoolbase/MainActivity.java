package com.equineelectronics.trimtoolbase;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.StyleableRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

public class MainActivity extends AppCompatActivity {
    private DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHandler = new DBHandler(this);
        SQLiteDatabase mSqLiteDatabase = dbHandler.getWritableDatabase();

        Cursor clientsCount = mSqLiteDatabase.rawQuery("select count(*) from " + dbHandler.CLIENTS_TABLE, null);
        clientsCount.moveToFirst();
        int countClients = clientsCount.getInt(0);
        clientsCount.close();

        TextView clientsValue = (TextView) findViewById(R.id.clientsValue);
        clientsValue.setText("" + countClients, TextView.BufferType.EDITABLE);

        Cursor horsesCount = mSqLiteDatabase.rawQuery("select count(*) from " + dbHandler.HORSES_TABLE, null);
        horsesCount.moveToFirst();
        int countHorses = horsesCount.getInt(0);
        horsesCount.close();

        TextView horsesValue = (TextView) findViewById(R.id.horsesValue);
        horsesValue.setText("" + countHorses, TextView.BufferType.EDITABLE);

        Cursor eventsCount = mSqLiteDatabase.rawQuery("select count(*) from " + dbHandler.EVENTS_TABLE, null);
        eventsCount.moveToFirst();
        int countEvents = eventsCount.getInt(0);
        eventsCount.close();

        TextView eventsValue = (TextView) findViewById(R.id.eventsValue);
        eventsValue.setText("" + countEvents, TextView.BufferType.EDITABLE);

        TypedValue paid = new TypedValue();
        @StyleableRes int paidIndex = 3;
        getResources().obtainTypedArray(R.array.event_statuses).getValue(paidIndex, paid);
        Cursor earnedCount = mSqLiteDatabase.rawQuery("select sum(" + dbHandler.EVENTS_AMOUNT_COLUMN + ") from " + dbHandler.EVENTS_TABLE + " WHERE " + dbHandler.EVENTS_STATUS_COLUMN + " like '" + paid.coerceToString() + "'", null);
        earnedCount.moveToFirst();
        float earnedSum = earnedCount.getFloat(0);
        earnedCount.close();

        TextView earnedValue = (TextView) findViewById(R.id.earnedValue);
        earnedValue.setText("" + earnedSum, TextView.BufferType.EDITABLE);

        TypedValue to_be_paid = new TypedValue();
        @StyleableRes int toBePaidIndex = 2;
        getResources().obtainTypedArray(R.array.event_statuses).getValue(toBePaidIndex, to_be_paid);
        Cursor toBePaidCount = mSqLiteDatabase.rawQuery("select sum(" + dbHandler.EVENTS_AMOUNT_COLUMN + ") from " + dbHandler.EVENTS_TABLE + " WHERE " + dbHandler.EVENTS_STATUS_COLUMN + " like '" + to_be_paid.coerceToString() + "'", null);
        toBePaidCount.moveToFirst();
        float toBePaidSum = toBePaidCount.getFloat(0);
        toBePaidCount.close();

        TextView toBePaidValue = (TextView) findViewById(R.id.toBePaidValue);
        toBePaidValue.setText("" + toBePaidSum, TextView.BufferType.EDITABLE);
        Log.i("toBePaidValue", " = " + toBePaidSum);
    }

    public void StartButtonClick(View view) {
        Intent intent = new Intent(MainActivity.this, ClientsActivity.class);
        startActivity(intent);
    }

    public void AllHorsesButtonClick(View view) {
        Intent intent = new Intent(MainActivity.this, HorsesActivity.class);
        startActivity(intent);
    }

    public void AllEventsButtonClick(View view) {
        Intent intent = new Intent(MainActivity.this, EventActivity.class);
        startActivity(intent);
    }

    private class DialogListener implements OpenFileDialog.OpenDialogListener {
        public void OnSelectedFile(String fileName) {
            String DBName = dbHandler.getDatabaseName();

            File data = Environment.getDataDirectory();
            String destinationPath = "//data//" + "com.equineelectronics.trimtoolbase" + "//databases//" + DBName;

            try {
                File from = new File(fileName);
                File to = new File(data, destinationPath);
                to.createNewFile();

                FileChannel src = new FileInputStream(from).getChannel();
                FileChannel dst = new FileOutputStream(to).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Toast.makeText(getApplicationContext(), "Database has been imported", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void ImportButtonClick(View view) {
        OpenFileDialog.OpenDialogListener listener = new DialogListener();

        OpenFileDialog fileDialog = new OpenFileDialog(this).setFilter(".*\\.sql").setOpenDialogListener(listener);
        fileDialog.show();
    }

    public void ExportButtonClick(View view) {
        String DBName = dbHandler.getDatabaseName();
        File sd = getApplicationContext().getExternalFilesDir(null);
        File data = Environment.getDataDirectory();
        String currentDBPath = "//data//" + "com.equineelectronics.trimtoolbase" + "//databases//" + DBName;

        try {
            File from = new File(data, currentDBPath);
            File to = new File(sd, DBName);
            to.createNewFile();

            FileChannel src = new FileInputStream(from).getChannel();
            FileChannel dst = new FileOutputStream(to).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
            Toast.makeText(getApplicationContext(), "Database has been saved in " + sd.getAbsolutePath(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
    }
}