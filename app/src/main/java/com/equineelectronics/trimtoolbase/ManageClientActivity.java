package com.equineelectronics.trimtoolbase;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ManageClientActivity extends AppCompatActivity {

    private DBHandler dbHandler;
    private SQLiteDatabase mSqLiteDatabase;
    private Long client_id;
    private EditText clientNameField;
    private EditText clientEmailField;
    private EditText clientPhoneField;
    private EditText clientCityField;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_client);

        dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        client_id = getIntent().getExtras().getLong("client_id");

        //Get client data
        String selection = BaseColumns._ID + " = ?";
        String selectionArgs[] = new String[] {"" + client_id};
        cursor = mSqLiteDatabase.query(
                dbHandler.CLIENTS_TABLE,
                new String[]{
                        BaseColumns._ID,
                        dbHandler.CLIENTS_NAME_COLUMN,
                        dbHandler.CLIENTS_EMAIL_COLUMN,
                        dbHandler.CLIENTS_PHONE_COLUMN,
                        dbHandler.CLIENTS_CITY_COLUMN,
                },
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        cursor.moveToFirst();

        //Set form values
        clientNameField = (EditText) findViewById(R.id.clientNameField);
        clientEmailField = (EditText) findViewById(R.id.clientEmailField);
        clientPhoneField = (EditText) findViewById(R.id.clientPhoneField);
        clientCityField = (EditText) findViewById(R.id.clientCityField);
        clientNameField.setText(cursor.getString(cursor.getColumnIndex(dbHandler.CLIENTS_NAME_COLUMN)), TextView.BufferType.EDITABLE);
        clientEmailField.setText(cursor.getString(cursor.getColumnIndex(dbHandler.CLIENTS_EMAIL_COLUMN)), TextView.BufferType.EDITABLE);
        clientPhoneField.setText(cursor.getString(cursor.getColumnIndex(dbHandler.CLIENTS_PHONE_COLUMN)), TextView.BufferType.EDITABLE);
        clientCityField.setText(cursor.getString(cursor.getColumnIndex(dbHandler.CLIENTS_CITY_COLUMN)), TextView.BufferType.EDITABLE);
    }

    public void deleteClientButtonClick(View view) {
        String selectionDelete = BaseColumns._ID + " = ?";
        String selectionDeleteArgs[] = new String[] {"" + client_id};
        mSqLiteDatabase.delete(dbHandler.CLIENTS_TABLE, selectionDelete, selectionDeleteArgs);

        //TODO delete all relative child records

        Intent answerIntent = new Intent();
        setResult(RESULT_OK, answerIntent);
        finish();
    }

    public void SaveClientButtonClick(View view) {
        //check if such email and name is not exist in db
        String selection = dbHandler.CLIENTS_NAME_COLUMN + " = ? OR " + dbHandler.CLIENTS_EMAIL_COLUMN + " = ?";
        String selectionArgs[] = new String[] {clientNameField.getText().toString(), clientEmailField.getText().toString()};
        Cursor cursorN = mSqLiteDatabase.query(
                dbHandler.CLIENTS_TABLE,
                new String[]{
                        BaseColumns._ID, dbHandler.CLIENTS_NAME_COLUMN, dbHandler.CLIENTS_EMAIL_COLUMN
                },
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        cursorN.moveToFirst();

        if (cursorN.getCount() > 0 && cursorN.getInt(cursor.getColumnIndex(BaseColumns._ID)) != cursor.getInt(cursor.getColumnIndex(BaseColumns._ID))) {
            Toast.makeText(getApplicationContext(),
                    "Such user is already exist, please choose other email and/or name", Toast.LENGTH_SHORT).show();
        } else {
            ContentValues values = new ContentValues();
            values.put(dbHandler.CLIENTS_NAME_COLUMN, clientNameField.getText().toString());
            values.put(dbHandler.CLIENTS_EMAIL_COLUMN, clientEmailField.getText().toString());
            values.put(dbHandler.CLIENTS_PHONE_COLUMN, clientPhoneField.getText().toString());
            values.put(dbHandler.CLIENTS_CITY_COLUMN, clientCityField.getText().toString());

            String selectionUpdate = BaseColumns._ID + " = ?";
            String selectionArgsUpdate[] = new String[] {"" + client_id};
            mSqLiteDatabase.update(dbHandler.CLIENTS_TABLE, values, selectionUpdate, selectionArgsUpdate);


            cursorN.close();
            cursor.close();
            Intent answerIntent = new Intent();
            setResult(RESULT_OK, answerIntent);
            finish();
        }
    }
}
