package com.equineelectronics.trimtoolbase;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ManageEventActivity extends AppCompatActivity {

    private DBHandler dbHandler;
    private SQLiteDatabase mSqLiteDatabase;
    private Long event_id;
    private Cursor cursor;
    private TextView eventDatePickerField;
    private Spinner EventTypeSpinnerField;
    private Spinner EventStatusSpinnerField;
    private EditText eventAmountField;
    private EditText eventGeneralDescriptionField;

    int DIALOG_DATE = 1;
    int myYear;
    int myMonth;
    int myDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_event);

        dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        event_id = getIntent().getExtras().getLong("event_id");

        //Get event data
        String selection = BaseColumns._ID + " = ?";
        String selectionArgs[] = new String[]{"" + event_id};
        cursor = mSqLiteDatabase.query(
                dbHandler.EVENTS_TABLE,
                new String[]{
                        BaseColumns._ID,
                        dbHandler.EVENTS_DATE_COLUMN,
                        dbHandler.EVENTS_TYPE_COLUMN,
                        dbHandler.EVENTS_STATUS_COLUMN,
                        dbHandler.EVENTS_HORSE_ID_COLUMN,
                        dbHandler.EVENTS_AMOUNT_COLUMN,
                        dbHandler.EVENTS_GENERAL_DESCRIPTION_COLUMN,
                },
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        cursor.moveToFirst();

        String dateString = cursor.getString(cursor.getColumnIndex(dbHandler.EVENTS_DATE_COLUMN));

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        try {
            calendar.setTime(sdf.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        myYear = calendar.get(Calendar.YEAR);
        myMonth = calendar.get(Calendar.MONTH);
        myDay = calendar.get(Calendar.DAY_OF_MONTH);

        //Set form values
        eventDatePickerField = (TextView) findViewById(R.id.eventDatePickerField);
        eventDatePickerField.setText(cursor.getString(cursor.getColumnIndex(dbHandler.EVENTS_DATE_COLUMN)), TextView.BufferType.EDITABLE);

        addEventTypesSpinner(cursor.getString(cursor.getColumnIndex(dbHandler.EVENTS_TYPE_COLUMN)));
        addEventStatusesSpinner(cursor.getString(cursor.getColumnIndex(dbHandler.EVENTS_STATUS_COLUMN)));

        eventAmountField = (EditText) findViewById(R.id.eventAmountField);
        eventAmountField.setText(cursor.getString(cursor.getColumnIndex(dbHandler.EVENTS_AMOUNT_COLUMN)), TextView.BufferType.EDITABLE);

        eventGeneralDescriptionField = (EditText) findViewById(R.id.eventGeneralDescriptionField);
        eventGeneralDescriptionField.setText(cursor.getString(cursor.getColumnIndex(dbHandler.EVENTS_GENERAL_DESCRIPTION_COLUMN)), TextView.BufferType.EDITABLE);
    }

    public void selectDate(View view) {
        showDialog(DIALOG_DATE);
    }

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_DATE) {
            DatePickerDialog tpd = new DatePickerDialog(this, myCallBack, myYear, myMonth, myDay);
            return tpd;
        }
        return super.onCreateDialog(id);
    }

    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String myMonthString;
            String myDayString;

            myYear = year;
            myMonth = monthOfYear + 1;
            myDay = dayOfMonth;
            if (myMonth < 10) {
                myMonthString = "0" + myMonth;
            } else {
                myMonthString = "" + myMonth;
            }

            if (myDay < 10) {
                myDayString = "0" + myDay;
            } else {
                myDayString = "" + myDay;
            }

            eventDatePickerField.setText(myYear + "/" + myMonthString + "/" + myDayString);
        }
    };

    public void deleteEventButtonClick(View view) {
        String selectionDelete = BaseColumns._ID + " = ?";
        String selectionDeleteArgs[] = new String[]{"" + event_id};
        mSqLiteDatabase.delete(dbHandler.EVENTS_TABLE, selectionDelete, selectionDeleteArgs);

        //TODO delete all relative child records

        Intent answerIntent = new Intent();
        setResult(RESULT_OK, answerIntent);
        finish();
    }

    public void SaveEventButtonClick(View view) {
        ContentValues values = new ContentValues();
        values.put(dbHandler.EVENTS_DATE_COLUMN, eventDatePickerField.getText().toString());
        values.put(dbHandler.EVENTS_TYPE_COLUMN, EventTypeSpinnerField.getSelectedItem().toString());
        values.put(dbHandler.EVENTS_STATUS_COLUMN, EventStatusSpinnerField.getSelectedItem().toString());
        values.put(dbHandler.EVENTS_AMOUNT_COLUMN, eventAmountField.getText().toString());
        values.put(dbHandler.EVENTS_GENERAL_DESCRIPTION_COLUMN, eventGeneralDescriptionField.getText().toString());

        String selectionUpdate = BaseColumns._ID + " = ?";
        String selectionArgsUpdate[] = new String[]{"" + event_id};
        mSqLiteDatabase.update(dbHandler.EVENTS_TABLE, values, selectionUpdate, selectionArgsUpdate);

        cursor.close();
        Intent answerIntent = new Intent();
        setResult(RESULT_OK, answerIntent);
        finish();
    }

    private void addEventTypesSpinner(String type) {
        // адаптер
        String[] types = getResources().getStringArray(R.array.event_types);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        EventTypeSpinnerField = (Spinner) findViewById(R.id.EventTypeSpinnerField);
        EventTypeSpinnerField.setAdapter(adapter);

        // заголовок
        EventTypeSpinnerField.setPrompt("Type");

        if (!type.equals(null)) {
            int spinnerPosition = adapter.getPosition(type);
            EventTypeSpinnerField.setSelection(spinnerPosition);
        }

        // устанавливаем обработчик нажатия
        EventTypeSpinnerField.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
//                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void addEventStatusesSpinner(String status) {
        // адаптер
        String[] statuses = getResources().getStringArray(R.array.event_statuses);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, statuses);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        EventStatusSpinnerField = (Spinner) findViewById(R.id.EventStatusSpinnerField);
        EventStatusSpinnerField.setAdapter(adapter);

        // заголовок
        EventStatusSpinnerField.setPrompt("Status");

        if (!status.equals(null)) {
            int spinnerPosition = adapter.getPosition(status);
            EventStatusSpinnerField.setSelection(spinnerPosition);
        }

        // устанавливаем обработчик нажатия
        EventStatusSpinnerField.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
//                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }
}
