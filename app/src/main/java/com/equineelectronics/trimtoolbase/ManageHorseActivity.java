package com.equineelectronics.trimtoolbase;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ManageHorseActivity extends AppCompatActivity {

    private SQLiteDatabase mSqLiteDatabase;
    private Long horse_id;
    private EditText horseNameField;
    private EditText horseYearOfBirthField;
    private Spinner breedSpinner;
    private Spinner sexSpinner;
    private CheckBox horseDeceasedField;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_horse);

        DBHandler dbHandler = new DBHandler(this);
        mSqLiteDatabase = dbHandler.getWritableDatabase();

        horse_id = getIntent().getExtras().getLong("horse_id");

        //Get horse data
        String selection = BaseColumns._ID + " = ?";
        String selectionArgs[] = new String[]{"" + horse_id};
        cursor = mSqLiteDatabase.query(
                DBHandler.HORSES_TABLE,
                new String[]{
                        BaseColumns._ID,
                        DBHandler.HORSES_NAME_COLUMN,
                        DBHandler.HORSES_YEAR_OF_BIRTH_COLUMN,
                        DBHandler.HORSES_BREED_COLUMN,
                        DBHandler.HORSES_CLIENT_ID_COLUMN,
                        DBHandler.HORSES_SEX_COLUMN,
                        DBHandler.HORSES_IS_DECEASED_COLUMN
                },
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        cursor.moveToFirst();

        //Set form values
        horseNameField = findViewById(R.id.horseNameField);
        horseYearOfBirthField = findViewById(R.id.horseYearOfBirthField);
        horseDeceasedField = findViewById(R.id.horseDeceasedField);

        horseNameField.setText(cursor.getString(cursor.getColumnIndex(DBHandler.HORSES_NAME_COLUMN)), TextView.BufferType.EDITABLE);
        horseYearOfBirthField.setText(cursor.getString(cursor.getColumnIndex(DBHandler.HORSES_YEAR_OF_BIRTH_COLUMN)), TextView.BufferType.EDITABLE);
        boolean isDeceased = "1".equals(cursor.getString(cursor.getColumnIndex(DBHandler.HORSES_IS_DECEASED_COLUMN)));
        horseDeceasedField.setChecked(isDeceased);

        addHorseSexSpinner(cursor.getString(cursor.getColumnIndex(DBHandler.HORSES_SEX_COLUMN)));
        addHorseBreedSpinner(cursor.getString(cursor.getColumnIndex(DBHandler.HORSES_BREED_COLUMN)));
    }

    private void addHorseBreedSpinner(String breed) {
        // адаптер
        String[] breeds = getResources().getStringArray(R.array.horse_breeds);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, breeds);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        breedSpinner = findViewById(R.id.horseBreedSpinnerField);
        breedSpinner.setAdapter(adapter);

        // заголовок
        breedSpinner.setPrompt("Breed");

        if (!breed.equals(null)) {
            int spinnerPosition = adapter.getPosition(breed);
            breedSpinner.setSelection(spinnerPosition);
        }

        // устанавливаем обработчик нажатия
        breedSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
                //Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void addHorseSexSpinner(String sex) {
        // адаптер
        String[] breeds = getResources().getStringArray(R.array.horse_sexs);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, breeds);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sexSpinner = findViewById(R.id.horseSexSpinnerField);
        sexSpinner.setAdapter(adapter);

        // заголовок
        sexSpinner.setPrompt("Sex");

        if (!sex.equals(null)) {
            int spinnerPosition = adapter.getPosition(sex);
            sexSpinner.setSelection(spinnerPosition);
        }

        // устанавливаем обработчик нажатия
        sexSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position, long id
            ) {
                // показываем позиция нажатого элемента
                //Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    public void deleteHorseButtonClick(View view) {
        String selectionDelete = BaseColumns._ID + " = ?";
        String selectionDeleteArgs[] = new String[]{"" + horse_id};
        mSqLiteDatabase.delete(DBHandler.HORSES_TABLE, selectionDelete, selectionDeleteArgs);

        //TODO delete all relative child records

        Intent answerIntent = new Intent();
        setResult(RESULT_OK, answerIntent);
        finish();
    }

    public void SaveHorseButtonClick(View view) {
        //check if such horse name is not exist in db for current client
        String selection = DBHandler.HORSES_NAME_COLUMN + " = ? AND " + DBHandler.HORSES_CLIENT_ID_COLUMN + " = ?";
        String selectionArgs[] = new String[]{horseNameField.getText().toString(), "" + cursor.getInt(cursor.getColumnIndex(DBHandler.HORSES_CLIENT_ID_COLUMN))};
        Cursor cursorN = mSqLiteDatabase.query(
                DBHandler.HORSES_TABLE,
                new String[]{
                        BaseColumns._ID, DBHandler.HORSES_NAME_COLUMN, DBHandler.HORSES_CLIENT_ID_COLUMN
                },
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        cursorN.moveToFirst();

        if (cursorN.getCount() > 0 && cursorN.getInt(cursor.getColumnIndex(BaseColumns._ID)) != cursor.getInt(cursor.getColumnIndex(BaseColumns._ID))) {
            Toast.makeText(getApplicationContext(),
                    "Such horse is already exist, please choose other name", Toast.LENGTH_SHORT).show();
        } else {
            ContentValues values = new ContentValues();
            values.put(DBHandler.HORSES_NAME_COLUMN, horseNameField.getText().toString());
            values.put(DBHandler.HORSES_YEAR_OF_BIRTH_COLUMN, horseYearOfBirthField.getText().toString());
            values.put(DBHandler.HORSES_BREED_COLUMN, breedSpinner.getSelectedItem().toString());
            values.put(DBHandler.HORSES_SEX_COLUMN, sexSpinner.getSelectedItem().toString());
            values.put(DBHandler.HORSES_IS_DECEASED_COLUMN, horseDeceasedField.isChecked());

            String selectionUpdate = BaseColumns._ID + " = ?";
            String selectionArgsUpdate[] = new String[]{"" + horse_id};
            mSqLiteDatabase.update(DBHandler.HORSES_TABLE, values, selectionUpdate, selectionArgsUpdate);

            cursorN.close();
            cursor.close();
            Intent answerIntent = new Intent();
            setResult(RESULT_OK, answerIntent);
            finish();
        }
    }
}
